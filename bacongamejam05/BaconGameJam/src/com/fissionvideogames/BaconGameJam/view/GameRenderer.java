package com.fissionvideogames.BaconGameJam.view;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;

public class GameRenderer
{

	OrthographicCamera camera;
	Box2DDebugRenderer debugRenderer;
	com.badlogic.gdx.physics.box2d.World world;

	public GameRenderer()
	{
		camera = new OrthographicCamera(48, 32);
		debugRenderer = new Box2DDebugRenderer();
	}

	public void render(float delta)
	{

		camera.update();
		debugRenderer.render(world, camera.combined);
	}

	public void SetupConeLight(Body body)
	{
		// light2 = new ConeLight(rayHandler, RAYS_PER_BALL, Color.GREEN,
		// LIGHT_DISTANCE, 0, 0, -90, 45);
	}

}
