package com.fissionvideogames.BaconGameJam.entities;

import box2dLight.ConeLight;
import box2dLight.Light;
import box2dLight.PointLight;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.fissionvideogames.BaconGameJam.controller.GameEngine;

public class Player extends Entity
{

	private static final int FRAME_COLS = 8;
	private static final int FRAME_ROWS = 1;
	Light light;
	Light playerlight;
	public float playerLightPower = 100;
	public Texture walkSheet;
	TextureRegion[] walkFrames;
	Animation walkAnimation;
	public boolean lightOn = true;
	float stateTime;
	TextureRegion currentFrame;

	public Player(Vector2 pos, GameEngine engine)
	{
		super(100, 33, 33, pos, engine);
		Color color = Color.GREEN;
		light = new ConeLight(engine.rayHandler,
				(int) (GameEngine.RAYS_PER_BALL), color, playerLightPower / 4,
				0, 0, -90, 55);
		playerlight = new PointLight(engine.rayHandler,
				GameEngine.RAYS_PER_BALL);
		playerlight.setDistance(2f);
		playerlight.attachToBody(body, 0, 0);
		light.attachToBody(body, 0, 0);
		light.setColor(1.0f, 0.95f, 0.2f, 1f);
		loadAnimations();
	}

	private void loadAnimations()
	{
		walkSheet = new Texture(
				Gdx.files.internal("assets/player/playeranimationsheet.png"));
		TextureRegion[][] tmp = TextureRegion.split(walkSheet,
				walkSheet.getWidth() / FRAME_COLS, walkSheet.getHeight()
						/ FRAME_ROWS); // #10
		walkFrames = new TextureRegion[FRAME_COLS * FRAME_ROWS];
		int index = 0;
		for (int i = 0; i < FRAME_ROWS; i++)
		{
			for (int j = 0; j < FRAME_COLS; j++)
			{
				walkFrames[index++] = tmp[i][j];
			}
		}
		walkAnimation = new Animation(.375f, walkFrames); // #11
		stateTime = 0f;
	}

	public boolean stopPlayerUpdatingCharacter=false;
	
	public void update(float delta)
	{
		body.setTransform(
				body.getTransform().getPosition(),
				(float) (Math.atan2(Gdx.input.getX() - Gdx.graphics.getWidth()
						/ 2, Gdx.input.getY() - Gdx.graphics.getHeight() / 2) - Math.PI / 2));

		Vector2 dir = new Vector2(0, 0);
		if(!stopPlayerUpdatingCharacter){
			if (Gdx.input.isKeyPressed(Input.Keys.W))
			{
				dir.y += 1;
			}
			if (Gdx.input.isKeyPressed(Input.Keys.A))
			{
				dir.x += -1;
			}
			if (Gdx.input.isKeyPressed(Input.Keys.S))
			{
				dir.y += -1;
			}
			if (Gdx.input.isKeyPressed(Input.Keys.D))
			{
				dir.x += 1;
			}
	
			if (Gdx.input.isButtonPressed(Buttons.LEFT))
			{
				light.setColor(1.0f, 0.95f, 0.2f, 1f);
				lightOn = true;
			} else if (Gdx.input.isButtonPressed(Buttons.RIGHT))
			{
				light.setColor(0f, 0f, 0f, 0f);
				lightOn = false;
			}
		}

		if (dir.len() != 0)
			dir.scl(1.0f / dir.len());

		walk(dir);
	}

	protected float getCollisionRadius()
	{
		return 26;
	}

	public void onDeath()
	{

	}

	public void setLightLength()
	{
		light.setDistance(playerLightPower / 4);
	}

	public TextureRegion getFrame()
	{
		stateTime += Gdx.graphics.getDeltaTime();
		currentFrame = walkAnimation.getKeyFrame(stateTime, true);
		return currentFrame;
	}

}