package com.fissionvideogames.BaconGameJam.entities;

import com.badlogic.gdx.math.Vector2;
import com.fissionvideogames.BaconGameJam.BaconUtil;
import com.fissionvideogames.BaconGameJam.BodyPositionable;
import com.fissionvideogames.BaconGameJam.Updatable;
import com.fissionvideogames.BaconGameJam.controller.GameEngine;

public abstract class Entity extends BodyPositionable implements Updatable
{
	public static byte UP = 0, RIGHT = 1, DOWN = 2, LEFT = 3;
	
	public int health, maxHealth;
	
	public GameEngine engine;
	public float speed;
	public float accel;
	
	public Entity(int maxHealth, float speed, float accel, Vector2 position, GameEngine engine) 
	{
		super(null);
		this.body = BaconUtil.createCircleBody(position.x, position.y, getCollisionRadius(),engine);
		this.health = this.maxHealth = maxHealth;
		this.engine = engine;
		this.engine.entities.add(this);
		this.speed = speed;
		this.accel = accel;
	}
	
	public void kill() 
	{
		health = 0;
		engine.physics.destroyBody(body);
		onDeath();
	}
	
	public Vector2 getDirection ()
	{
		return new Vector2((float)Math.cos(body.getTransform().getRotation()),(float)Math.sin(body.getTransform().getRotation()));
	}
	
	public void walk(Vector2 direction) 
	{
		direction = new Vector2(direction);
		direction.nor();
		Vector2 dirvel = direction.scl(speed);
		Vector2 dvel = dirvel.sub(body.getLinearVelocity());
		if (dvel.len2() > accel * accel)
		{
			dvel.scl(speed/dvel.len());
		}
		
		body.applyLinearImpulse(dvel,body.getWorldCenter(),true);
	}
	
	public void walkTowards (Vector2 position)
	{
		walk(new Vector2(position).sub(body.getPosition ()));
	}
	
	public void faceDirection (Vector2 dir)
	{
		float angle = dir.angle() * ((float)Math.PI/180);
		body.getTransform().setRotation(angle);
		body.setAngularVelocity(0);
	}
	
	public void facePosition(Vector2 pos)
	{
		pos = new Vector2(pos);
		Vector2 dir = (pos.sub(body.getPosition()));
		faceDirection(dir);
	}
	
	
	protected abstract float getCollisionRadius();
	public abstract void onDeath();

}