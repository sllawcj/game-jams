package com.fissionvideogames.BaconGameJam.entities;

import com.badlogic.gdx.math.Vector2;
import com.fissionvideogames.BaconGameJam.controller.GameEngine;

public class Battery extends Entity {

	public Battery(Vector2 position, GameEngine engine) {
		super(1, 0, 0, position, engine);
		// TODO Auto-generated constructor stub
		this.body.setActive(false);
	}

	@Override
	public void update(float delta) {
		if(distance(this.body.getPosition(), engine.player.body.getPosition()) < 3) {
			engine.player.playerLightPower += 30;
			if(engine.player.playerLightPower > 120) {
				engine.player.playerLightPower = 120;
			}
			engine.removeEntities.add(this);
		}
	}
	
	public float distance(Vector2 d1, Vector2 d2) {
		return (float) Math.sqrt((d1.x - d2.x) * (d1.x - d2.x) + (d1.y - d2.y) * (d1.y - d2.y));
	}


	@Override
	protected float getCollisionRadius() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void onDeath() {
		// TODO Auto-generated method stub
		
	}
	
	

}