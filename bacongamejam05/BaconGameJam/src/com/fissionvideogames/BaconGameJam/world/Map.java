package com.fissionvideogames.BaconGameJam.world;

import java.util.ArrayList;
import java.util.Random;

import pathfinding.GridMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.fissionvideogames.BaconGameJam.BaconUtil;
import com.fissionvideogames.BaconGameJam.controller.GameEngine;
import com.fissionvideogames.BaconGameJam.entities.Battery;
import com.fissionvideogames.BaconGameJam.entities.Player;

public class Map
{
	public Room root;

	public Random rand = new Random();
	private int subdivisions = 10;
	private int maxroom = 200;
	private int minroom = 160;
	private int dimensions = 160;

	public boolean[][] blocks = new boolean[dimensions][dimensions];
	public short[][] textures = new short[dimensions][dimensions];
	
	public GridMap paths = new GridMap(dimensions, dimensions);
	
	public static TextureRegion blockTexture  = new TextureRegion(new Texture(Gdx.files.internal("assets/block.png")));
	TextureRegion blockTexture1 = new TextureRegion(new Texture(Gdx.files.internal("assets/1.png")));
	TextureRegion blockTexture2 = new TextureRegion(new Texture(Gdx.files.internal("assets/2.png")));
	TextureRegion blockTexture3 = new TextureRegion(new Texture(Gdx.files.internal("assets/3.png")));
	TextureRegion blockTexture4 = new TextureRegion(new Texture(Gdx.files.internal("assets/4.png")));
	TextureRegion blockTexture5 = new TextureRegion(new Texture(Gdx.files.internal("assets/5.png")));
	TextureRegion blockTexture6 = new TextureRegion(new Texture(Gdx.files.internal("assets/6.png")));
	TextureRegion blockTexture7 = new TextureRegion(new Texture(Gdx.files.internal("assets/7.png")));
	TextureRegion blockTexture8 = new TextureRegion(new Texture(Gdx.files.internal("assets/8.png")));	
	TextureRegion blockTest = new TextureRegion(new Texture(Gdx.files.internal("assets/blackbackground.png")));
	
	ArrayList<Body> bodies = new ArrayList<Body>();
	
	GameEngine engine;
	public ThroughPathGen throughPathGen;
	
	public Map (GameEngine engine)
	{
		this.engine = engine;
		throughPathGen = new ThroughPathGen(this);
	}
	
	/**
	 * 
	 * @return The starting position as [0] and [1], and the end goal as [2], [3].
	 */
	public int[] generate()
	{
		
		generateTextureMap();
		
		paths = new GridMap(dimensions, dimensions);

		PerlinNoise p = new PerlinNoise();

		//System.out.println("Generating world...");

		fillAll();

		root = new Room(0, 0, dimensions, dimensions,this);
		root.subdivisions = subdivisions;
		root.split(true);

		ArrayList<Room> rooms = root.getFinalChildren();
		int roomcount = rand.nextInt(maxroom - minroom) + minroom;
		ArrayList<Room> undeleted = new ArrayList<Room>(16);
		
		Room start = getStartingRoom();
		Room end = getEndingRoom();


		for (int i = 0; i < roomcount; i++)
		{
			undeleted.add(rooms.get(rand.nextInt(rooms.size())));
		}
		
		undeleted.add(start);
		undeleted.add(end);

		rooms = undeleted;
		


		for (Room r : rooms)
		{
			clearRoom(r);
		}
		
		for(Room r : rooms) {
			ArrayList<Room> nearby = new ArrayList<Room>(3);
			int distance = 0;
			for(Room r2 : rooms) {
				if(r != r2) {
					if(distance(r.getRoomCenter(), r2.getRoomCenter()) < distance) {
						nearby.add(r2);
					}
				}
			}
			if(rand.nextInt(2) == 1 || r == start || r == end) {
				Room near = rooms.get(rand.nextInt(rooms.size()));
				if(distance(near.getRoomCenter(), r.getRoomCenter()) < 100 || r == end || r == start)
					nearby.add(near);
			}
			
			for(Room r2 : nearby)
				fillConnection(r, r2);
		}

		
		for (int x = 0; x < dimensions; x++)
		{
			for (int z = 0; z < dimensions; z++)
			{
				if (p.noiseOct(x + 0.5f, 0.5f, z + 0.5f, 3, 15) >= 0.9f)
				{
					blocks[x][z] = false;
				}
				if (p.noiseOct(x + 0.5f, 9.5f, z + 0.5f, 3, 15) >= 0.9f)
				{
					blocks[x][z] = false;
				}
				if (p.noiseOct(x + 0.5f, 0.9f, z + 0.5f, 3, 7) <= 0.6f)
				{
					blocks[x][z] = true;
				}

			}
		}
		
		clearRoom(start);
		clearRoom(end);
		
		ArrayList<Room> startend = new ArrayList<Room>(16);
		startend.add(start);
		startend.add(end);
		
		for(Room r : startend) {
			ArrayList<Room> nearby = new ArrayList<Room>(3);
			int distance = 0;
			for(Room r2 : rooms) {
				if(r != r2) {
					if(distance(r.getRoomCenter(), r2.getRoomCenter()) < distance) {
						nearby.add(r2);
					}
				}
			}
			if(rand.nextInt(2) == 1 || r == start || r == end) {
				Room near = rooms.get(rand.nextInt(rooms.size()));
				if(distance(near.getRoomCenter(), r.getRoomCenter()) < 100 || r == end || r == start)
					nearby.add(near);
			}
			
			for(Room r2 : nearby)
				fillConnection(r, r2);
		}
		
		fillOutsides(new Room(0, 0, dimensions - 1, dimensions - 1, this));

		//System.out.println("\n\n\n");
		printBuilding();
		
		for(int x = 0; x < dimensions; x++) {
			for(int z = 0; z < dimensions; z++) { 
				paths.set(x, z, blocks[x][z] == true ? GridMap.WALL : 1);
			} 
		}
		throughPathGen.doSetup();	
		GenerateBodies();
		
		generateBatteries();

			
		return new int[] {start.x1 + 3, start.z1 + 3, end.x1 + 3, end.z1 + 3};
		
	}
	
	private void generateBatteries() {
		for(int x = 0; x < dimensions; x++) {
			for(int y = 0; y < dimensions; y++) {
				if(!isFilled(x, y) && rand.nextInt(75) == 8) {
					engine.entities.add(new Battery(new Vector2(x * 2 + 1, y * 2 + 1), engine));
				}
			}
		}
	}
	
	private void generateTextureMap() {
		for(int x = 0; x < dimensions; x++) {
			for(int y = 0; y < dimensions; y++) {
				if(rand.nextInt(30) == 8) {
					textures[x][y] = (short) (2 + rand.nextInt(7));
				}
				else {
					textures[x][y] = 1;
				}
			}
		}
	}
	
	private void fillOutsides(Room r) {
		for(int i = r.x1; i <= r.x2; i++) {
			if(r.z1 >= 0 && r.z2 - 1 >= 0) {
				blocks[i][r.z1] = true;
				blocks[i][r.z2] = true;
			}
		}
		for(int i = r.z1; i <= r.z2; i++) {
			blocks[r.x1][i] = true;
			blocks[r.x2][i] = true;
		}
	}
	
	private Room getStartingRoom() {
		int x = rand.nextInt(30) + 10;
		int x2 = x + rand.nextInt(10) + 6;
		int z = rand.nextInt(10) + 10;
		int z2 = rand.nextInt(10) + 6 + z;
		Room r = new Room(10, 10, 16, 16, this);
		return r;
	}
	
	private Room getEndingRoom() {
		int x = rand.nextInt(30) + 10;
		int x2 = x + rand.nextInt(10) + 6;
		int z = dimensions - rand.nextInt(5) - 20;
		int z2 = rand.nextInt(10) + 6 + z;
		Room r = new Room(dimensions - 26, dimensions - 26, dimensions - 4, dimensions - 4, this);
		return r;
	}
	
	private void diagonalConnection(int x1, int x2, int y1, int y2) {
		float sY = y2 - y1;
		float sX = x2 - x1;
		Vector2 slope = new Vector2(sX, sY);
		slope.nor();
		
		float x = x1;
		float y = y1;
		while(distance(new int[] {(int) x, (int) y}, new int[] {x2, y2}) > 3) {
			try {
				x += slope.x;
				y += slope.y;
				blocks[(int) x][(int) y] = false;
				blocks[(int) x + 1][(int) y] = false;
				blocks[(int) x + 1][(int) y + 1] = false;
			}
			catch(ArrayIndexOutOfBoundsException except) {
				except.printStackTrace();
			}
		}
		
	}
	
	public void fillConnection(Room r1, Room r2) {
		int cx1 = r1.getRoomCenter()[0];
		int cx2 = r2.getRoomCenter()[0];
		int cy1 = r1.getRoomCenter()[1];
		int cy2 = r2.getRoomCenter()[1];		
		
		if(rand.nextInt(3) == 0) {
			if(cx1 > cx2) {
				int c = cx1;
				cx1 = cx2;
				cx2 = c;
			}
			
			if(cy1 > cy2) {
				int c = cy1;
				cy1 = cy2;
				cy2 = c;
			}
			xstuff(cx1, cx2, cy1, cy2, true);
			ystuff(cx1, cx2, cy1, cy2, false);
		}
		else if(rand.nextBoolean()) {
			if(cx1 > cx2) {
				int c = cx1;
				cx1 = cx2;
				cx2 = c;
			}
			
			if(cy1 > cy2) {
				int c = cy1;
				cy1 = cy2;
				cy2 = c;
			}
			xstuff(cx1, cx2, cy1, cy2, false);
			ystuff(cx1, cx2, cy1, cy2, true);
		}	
		else {		
			diagonalConnection(cx1, cx2, cy1, cy2);
		}
		
		
	}
	
	private void xstuff(int cx1, int cx2, int cy1, int cy2, boolean first) {
		for(int x = cx1; x <= cx2; x += 1) {
			if(first)
				for(int y = cy1; y <= cy1 + 1; y++) {
					if(x >= 0 && y >= 0 && x < dimensions && y < dimensions)
						blocks[x][y] = false;
				}
			else {
				for(int y = cy2; y <= cy2 + 1; y++) {
					if(x >= 0 && y >= 0 && x < dimensions && y < dimensions)
						blocks[x][y] = false;
				}
			}
		}
	}
	
	private void ystuff(int cx1, int cx2, int cy1, int cy2, boolean first) {
		for(int y = cy1; y <= cy2; y += 1) {
			if(first)
				for(int x = cx2 - 1; x < cx2; x++) {
					if(x >= 0 && y >= 0 && x < dimensions && y < dimensions)
						blocks[x][y] = false;
				}
			else {
				for(int x = cx1 - 1; x < cx1; x++) {
					if(x >= 0 && y >= 0 && x < dimensions && y < dimensions)
						blocks[x][y] = false;
				}
			}
		}
	}
	
	public float distance(int[] d1, int[] d2) {
		return (float) Math.sqrt((d1[0] - d2[0]) * (d1[0] - d2[0]) + (d1[1] - d2[1]) * (d1[1] - d2[1]));
	}

	public boolean drawPathTextures = false;
	public void Draw ()
	{
		int sx = (int)(engine.camera.position.x - engine.camera.viewportWidth/2)/2;
		int sy = (int)(engine.camera.position.y - engine.camera.viewportHeight/2)/2;
		int w = (int)(engine.camera.viewportWidth + 4)/2;
		int h = (int)(engine.camera.viewportHeight + 4)/2;

		
		for (int y = sy; y < sy+h; ++y)
		{
			for (int x = sx; x < sx + w;++x)
			{
				if (x >= 0 && x < blocks[0].length && y >= 0 && y < blocks.length)
				{
					if (isFilled(x,y))
					{
						engine.batch.draw(blockTexture,x*2, y * 2,2,2);
					}
					else if(!throughPathGen.throughPath[y][x] && drawPathTextures){
						engine.batch.draw(blockTest,x*2, y * 2,2,2);
					}
					else
					{
						switch(textures[x][y]) {
						case 1:
							engine.batch.draw(blockTexture1,x*2, y * 2,2,2);
							break;
						case 2:
							engine.batch.draw(blockTexture2,x*2, y * 2,2,2);
							break;
						case 3:
							engine.batch.draw(blockTexture3,x*2, y * 2,2,2);
							break;
						case 4:
							engine.batch.draw(blockTexture4,x*2, y * 2,2,2);
							break;
						case 5:
							engine.batch.draw(blockTexture5,x*2, y * 2,2,2);
							break;
						case 6:
							engine.batch.draw(blockTexture6,x*2, y * 2,2,2);
							break;
						case 7:
							engine.batch.draw(blockTexture7,x*2, y * 2,2,2);
							break;
						case 8:
							engine.batch.draw(blockTexture8,x*2, y * 2,2,2);
							break;
						}
					}
				}
			}
		}
	}
	
	private void GenerateBodies ()
	{
		boolean[][] mask = new boolean [blocks.length][];
		for (int y = 0; y < blocks.length; ++y)
		{
			mask[y] = blocks[y].clone();
		}
		
		for (int x = 0; x < blocks.length; x++)
		{
			for (int y = 0; y < blocks[0].length; y++)
			{
				if (!mask[y][x])
				{
					continue; //No face, no body.
				}
				
				int quadwidth = 0;
				for (quadwidth = 1; x + quadwidth < blocks[0].length && mask[y][x + quadwidth]; ++quadwidth)
				{}
				
				int quadheight = 0;
				boolean done = false;
				
				for (quadheight = 1; y + quadheight < blocks.length; ++quadheight)
				{
					for (int k = 0; k < quadwidth; ++k)
					{
						if (!mask[y + quadheight][x+k])
						{
							done = true;
							break;
						}
					}
					if (done)
						break;
				}
				
				
				bodies.add(BaconUtil.createStaticRectangle(x * 2, y * 2, quadwidth * 2, quadheight * 2, engine));
				
				
				for (int i = 0; i < quadwidth; ++i)
				{
					for (int j = 0; j < quadheight; ++j)
					{
						mask[y + j][x + i] = false;
					}
				}
			}
		}
	}
	
	/**
	 * Fills all space in the grid to true
	 */
	private void fillAll()
	{
		for (int x = 0; x < blocks.length; x++)
		{
			for (int z = 0; z < blocks[0].length; z++)
			{
				blocks[x][z] = true;
			}
		}
	}

	/**
	 * Debug printing of building
	 */
	private void printBuilding()
	{
		for (int x = 0; x < blocks.length; x++)
		{
			for (int z = 0; z < blocks[0].length; z++)
			{
				if (blocks[x][z] == false)
				{
					//System.out.print(" ");
				} else
				{
					//System.out.print("#");
				}
			}
			//System.out.println("\t" + x);
		}
	}

	/**
	 * Removes space from the grid when a room is cleared out
	 * 
	 * @param r
	 *            The room which will be cleared out
	 */
	private void clearRoom(Room r)
	{
		for (int x = r.x1; x < r.x2; x++)
		{
			for (int z = r.z1; z < r.z2; z++)
			{
				if (x >= 0 && z >= 0 && x < dimensions && z < dimensions)
					blocks[x][z] = false;
			}
		}
	}

	public Vector2 getEmptySpaceToStart()
	{
		/*
		for (int x = 5; x < blocks.length; x++)
		{
			for (int z = 20; z < blocks[0].length; z++)
			{
				if (isFilled(x,z) == false)
				{
					//System.out.print("Starting position:" + x + "," + (blocks[0].length-z));
					//return new Vector2(x, blocks[0].length-z);
				}
			}
		}
		 */
		return new Vector2(-2,-2);
	}

	public boolean isFilled(int x, int y)
	{
		if (x < 0 || x >= blocks[0].length)
			return true;
		if (y < 0 || y >= blocks.length)
			return true;
		return blocks[y][x];
	}
}
