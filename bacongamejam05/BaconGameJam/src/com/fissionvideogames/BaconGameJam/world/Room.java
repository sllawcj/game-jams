package com.fissionvideogames.BaconGameJam.world;

import java.util.ArrayList;

public class Room 
{

	public Room parent;
	public Room[] children;

	int x1;	//0
	int z1;	//1
	int x2;	//2
	int z2;	//3

	int subdivisions = 0;
	Map map;
	
	
	public Room(int x1, int z1, int x2, int z2, Map map) 
	{
		this.x1 = x1;
		this.x2 = x2;
		this.z1 = z1;
		this.z2 = z2;
		this.map = map;
	}

	public Room(int x1, int z1, int x2, int z2, int subdivisions, Map map)
	{
		this.x1 = x1;
		this.x2 = x2;
		this.z1 = z1;
		this.z2 = z2;
		this.subdivisions = subdivisions;
		this.map = map;
	}

	public void split(boolean recurse) {
		
		boolean canSplit = true;
		if(subdivisions == 0) {
			canSplit = false;
		}
		if(children == null) {
			while(canSplit) {
				if(subdivisions > 0 && children == null) {
					subdivisions--;
					splitMath();
					if(recurse) {
						for(Room r : children) {
							r.split(true);
						}
					}				
					canSplit = false;
				}
				else {
					//If not selected to be split, subtract one from subdivisions and repeat
					subdivisions--;
					children = null;
					if(subdivisions > 0)
						canSplit = true;
					else
						canSplit = false;
				}
			}			
		}
		else {
			subdivisions--;
			for(Room r : children) {
				r.split(true);
			}
		}
	}

	private void splitMath() {
		children = new Room[2];

		Room sd1 = null;
		Room sd2 = null;

		int rand = map.rand.nextInt(6);
		if((x2 - x1 > 1.2f * (z2 - z1))) {
			rand = map.rand.nextInt(3) + 3;
		}
		else if(((x2 - x1) * 1.2f < (z2 - z1))) {
			rand = map.rand.nextInt(3);
		}
		//More likely to be square-ish than long and skinny
		//Determines horizontal half, horizontal third, vertical half or vertical third
		switch(rand) {
		case 0:
			//Half one
			sd1 = new Room (x1, z1, x2, z1 + (z2 - z1) / 2,map);
			sd2 = new Room (x1, sd1.z2, x2, z2,map);
			break;

		case 1:
			//Third one
			sd1 = new Room (x1, z1, x2, z1 + (z2 - z1) / 3,map);
			sd2 = new Room (x1, sd1.z2, x2, z2,map);
			break;
		case 2:
			//Third three
			sd1 = new Room (x1, z1, x2, z1 + ((z2 - z1) / 3) * 2,map);
			sd2 = new Room (x1, sd1.z2, x2, z2,map);
			break;


		case 3: 
			//Half two
			sd1 = new Room (x1, z1, x1 + (x2 - x1) / 2, z2,map);
			sd2 = new Room (sd1.x2, z1, x2, z2,map);
			break;
		case 4: 
			//Third two
			sd1 = new Room (x1, z1, x1 + (x2 - x1) / 3, z2,map);
			sd2 = new Room (sd1.x2, z1, x2, z2,map);
			break;
		case 5:
			//Third four
			sd1 = new Room (x1, z1, x1 + ((x2 - x1) / 3) * 2, z2,map);
			sd2 = new Room (sd1.x2, z1, x2, z2,map);
			break;
		}

		sd1.parent = this;
		sd2.parent = this;
		sd1.subdivisions = subdivisions;
		sd2.subdivisions = subdivisions;
		children[0] = sd1;
		children[1] = sd2;
	}

	/**
	 * @return Approximate center point as X and Z
	 */
	public int[] getRoomCenter() {
		return new int[] {x1 + (x2 - x1) / 2, z1 + (z2 - z1) / 2};
	}

	public ArrayList<Room> getFinalChildren() {
		ArrayList<Room> finalChild = new ArrayList<Room>(1);
		if(this.children != null) {
			for(Room r : children) {
				if(r.children == null ) {
					finalChild.add(r);
				}
				else {
					finalChild.addAll(r.getFinalChildren());
				}
			}
		}
		else {
			finalChild.add(this);
		}
		
		return finalChild;
	}
	
	public Room getFinalParent() {
		Room current = this;
		while(current.parent != null) {
			current = current.parent;
		}
		return current;
	}

	public static boolean areTouching(Room r1, Room r2) {

		if(r1.x1 == r2.x1) {
			if( ( ( r1.z1 >= r2.z1 && r1.z1 <= r2.z2 ) || ( r1.z2 ) >= r2.z1 && r1.z2 <= r2.z2 ) ) {
				return true;
			}
		}

		else if(r1.z1 == r2.z1) {
			if( ( ( r1.x1 >= r2.z1 && r1.x1 <= r2.x2 ) || ( r1.x2 ) >= r2.x1 && r1.x2 <= r2.x2 ) ) {
				return true;
			}
		}

		else if(r1.x2 == r2.x2) {
			if( ( ( r1.z1 >= r2.z1 && r1.z1 <= r2.z2 ) || ( r1.z2 ) >= r2.z1 && r1.z2 <= r2.z2 ) ) {
				return true;
			}
		}

		else if(r1.z2 == r2.z2) {
			if( ( ( r1.x1 >= r2.z1 && r1.x1 <= r2.x2 ) || ( r1.x2 ) >= r2.x1 && r1.x2 <= r2.x2 ) ) {
				return true;
			}
		}


		return false;
	}

	/**
	 * Creates a root room with the minimum amount of children to always recreate a specific room
	 * @return The root room for the next floor up
	 */
	public Room getMinimumParentRoot() {

		Room current = this;
		boolean root = false;

		if(current.parent == null) {
			root = true;
		}

		while(!root) {

			if(current.parent.children[0] != current) {
				current.parent.children[0].children = null;
				//If the other child of the current's parent has no children, it must recreate them!
				//Basically, kill their brother's children. Think of it that way.
			}
			else {
				current.parent.children[1].children = null;
			}
			current = current.parent;
			if(current.parent == null) {
				root = true;
			}
		}

		return current;
	}
	
	private void printDebugTree(int subdiv, String parent) {
		//System.out.println("Room at " + parent + " with subdiv " + subdiv);
		subdiv++;
		if(this.children != null) {
			children[0].printDebugTree(subdiv, parent + "A ");
			children[0].printDebugTree(subdiv, parent + "B ");
		}
	}
	
	public void printDebugTree() {
		printDebugTree(0, "");
	}

	public int squareAreaXZ() {
		return (x2 - x1) * (z1 - z1);
	}

}
