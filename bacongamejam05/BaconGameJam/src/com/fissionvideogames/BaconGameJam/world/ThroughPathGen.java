package com.fissionvideogames.BaconGameJam.world;

import java.util.Random;

public class ThroughPathGen {

	public boolean[][] throughPath;
	int startx = 0;
	int starty = 0;
	public int endx, endy;
	int intervalCount = 6;
	int bufferfromborder = 4;
	Random random;
	Map map;
	public ThroughPathGen(Map map){
		this.map = map;
		//intervalCount = (int)map.blocks[0].length/5;
		throughPath = new boolean[map.blocks.length][map.blocks[0].length];
		for(int y = 0; y < throughPath.length; y++){
			for(int x = 0; x<throughPath[0].length; x++){
				throughPath[x][y] = true; //filed with block is true
			}
		}
		endx = map.blocks.length-1;
		endy = map.blocks[0].length-1;
		random = new Random();		
	}
	
	public void doSetup(){
		genPath();
		setPath();
	}
	
	int halfpoint;
	private int returnRandomX(int currentX){
		int result = 0;				
		if(currentX < halfpoint){
			result = currentX + random.nextInt(halfpoint);
			if(result > throughPath.length-1){
				result = throughPath.length-bufferfromborder;
			}
		}else{
			result = currentX - random.nextInt(halfpoint);
			if(result <=0){
				result = bufferfromborder;
			}
		}
		
		return result;
	}
	
	public void genPath(){
		halfpoint = throughPath.length/4;
	//	System.out.println("Halfpoint : " + halfpoint);
		int Ylength = throughPath[0].length - 10;
		int randomXlength = throughPath.length/3*2 - (bufferfromborder*2);
		int interval = (int)(Ylength/intervalCount);
		int currentX = startx; 
		int currentY = starty;
		for(int i = 1; i<intervalCount; i++){
						
			int wayPointX =  currentX ;
			while(wayPointX == currentX){
				wayPointX = returnRandomX(currentX); //bufferfromborder + random.nextInt(randomXlength);
			}
			int wayPointY = i*interval;
			//System.out.println("Current "+currentX+","+currentY+"  Waypoint "+ i + ": "+wayPointX+","+wayPointY );
			float m = (float) ( (float)(wayPointY - currentY)/ (float)(wayPointX -currentX) );
			float c = wayPointY - (m*wayPointX);						
			MakePath(currentX, currentY, wayPointX, wayPointY, m, c);											
			currentX = wayPointX;
			currentY = wayPointY;
			
		}
		endx = returnRandomX(currentX);
		float m = (float) ( (float)(endy - currentY)/ (float)(endx -currentX) );
		float c = currentY - (m*currentX);	
		MakePath(currentX, currentY, endx-1, endy-1,m,c);
		
		//System.out.println("\n\n\n\n");
		//map.printBuilding();
		printfull();
	}
	
	private void MakePath(int currentX, int currentY, int wayPointX, int wayPointY, float m, float c){
		int xiter_currentx = currentX;
		int xiter_currenty = currentY;
		int modifier = 1;
		boolean done = false;
		if(currentX > wayPointX)
			modifier = -1;
		int modifiery = 1;
		if(currentY > wayPointY)
			modifiery = -1;
		while(!done){
			if(xiter_currentx <0 || xiter_currentx > throughPath.length)
				done = true;				
			if(xiter_currentx == wayPointX)
				done=true;
			//y = mx + c
			xiter_currentx += modifier;
			xiter_currenty = (int)(m* (float)xiter_currenty + c);				
			//System.out.println(xiter_currentx+","+xiter_currenty+","+m);
			if(xiter_currenty >=0 && xiter_currenty < throughPath.length){
				//System.out.println("XAX x:"+ xiter_currentx + " y:"+ xiter_currenty+ " modifier:"+ modifier+" waypoint:"+wayPointX);
				throughPath[xiter_currenty][xiter_currentx] = false;
				if(xiter_currenty < throughPath.length -1)
					throughPath[xiter_currenty+1][xiter_currentx] = false;
				if(xiter_currenty > 0)
					throughPath[xiter_currenty-1][xiter_currentx] = false;
			}
		}	
		done = false;
		
		int yiter_currentx = currentX;
		int yiter_currenty = currentY;
		while(!done){
			if(yiter_currenty<0 || yiter_currenty > throughPath[0].length)
				done = true;
			if(yiter_currenty == wayPointY)
				done = true;
			yiter_currenty += modifiery;
			yiter_currentx = (int)( (float)(yiter_currenty - c)/(float)m);
			//System.out.println(yiter_currentx+","+yiter_currenty+","+m);
			if(yiter_currentx >=0 && yiter_currentx < throughPath[0].length){
				//System.out.println(" YAY x:"+ xiter_currentx + " y:"+ xiter_currenty+ " modifier Y:"+ modifiery+" waypointX:"+wayPointX);
				throughPath[yiter_currenty][yiter_currentx] = false;				
				if(yiter_currentx < throughPath[0].length -1)
					throughPath[yiter_currenty][yiter_currentx+1] = false;
				if(yiter_currentx >0)
					throughPath[yiter_currenty][yiter_currentx-1] = false;
			}
		}
	}
	
	private void printfull(){
		
		for (int x = 0; x < map.blocks.length; x++)
		{
			for (int z = 0; z < map.blocks[0].length; z++)
			{
				if(throughPath[x][z] == false){
					//System.out.print("-");
				}
				else{
					if (map.blocks[x][z] == false)
					{
						//System.out.print(" ");
					} else
					{
						//System.out.print("#");
					}
				}
			}
			//System.out.println("\t" + x);
		}
	}
	
	public void setPath(){
		for(int y = 0; y < throughPath.length; y++){
			for(int x = 0; x<throughPath[0].length; x++){
				if(throughPath[y][x] == false)
					this.map.blocks[y][x] = false; 
			}
		}
	}
	
	
	
}
