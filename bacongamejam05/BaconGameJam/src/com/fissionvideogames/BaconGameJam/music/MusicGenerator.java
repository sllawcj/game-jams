package com.fissionvideogames.BaconGameJam.music;

import java.util.ArrayList;
import java.util.Collections;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class MusicGenerator
{
	ArrayList<Music> ambient = new ArrayList<Music>();

	ArrayList<Music> epic = new ArrayList<Music>();

	FileHandle file;

	public MusicGenerator(FileHandle file)
	{
		this.file = file;
		loadAmbient(file);
		loadEpic(file);
	}

	private void loadEpic(FileHandle file)
	{
		for (int i = 1; i <= 4; i++)
		{
			try
			{
				epic.add(Gdx.audio.newMusic(file.child( "E" + i + ".wav")));
			} catch (GdxRuntimeException e)
			{
				
				//System.out.println("Track " + i + " failed to load!");
			}
		}

		Collections.shuffle(epic);

	}
	private void loadAmbient(FileHandle file)
	{
		for (int i = 1; i <= 4; i++)
		{
			try
			{
				ambient.add(Gdx.audio.newMusic(file.child("A" + i + ".wav")));
			}catch (GdxRuntimeException e)
			{
				//System.out.println("Track " + i + " failed to load!");
			}	
		}

		Collections.shuffle(ambient);
	}

	public Music getNextAmbient()
	{
		if (ambient.size() > 0)
		{
			Music next = ambient.get(0);
			ambient.remove(0);
			return next;
		}
		else
		{
			loadAmbient(file);
			return getNextAmbient();
		}
	}

	public Music getNextEpic()
	{
		if (epic.size() > 0)
		{
			Music next = epic.get(0);
			epic.remove(0);
			return next;
		} else
		{
			loadEpic(file);
			return getNextEpic();
		}

	}
}
