package com.fissionvideogames.BaconGameJam.eventsystem;

import com.fissionvideogames.BaconGameJam.controller.GameEngine;



public abstract class EventListener<T extends Event> 
{
	public boolean armed = true;
	private Class<T> clss;
	GameEngine engine;
	
	public EventListener(Class<T> clss, GameEngine engine) 
	{
		this.clss = clss;
		this.engine = engine;
		engine.events.addListener(clss, this);
	}
	
	public EventListener(Class<T> clss, boolean armed, GameEngine engine) {
		this(clss,engine);
		this.armed = armed;
	}
	
	public void arm() {
		armed = true;
	}
	
	public void disarm() {
		armed = false;
	}
	
	public void destroy() 
	{
		engine.events.getListeners(clss).remove(this);
	}
	
	public abstract void beforeEvent (T e);
	public abstract void afterEvent  (T e);
	 
}