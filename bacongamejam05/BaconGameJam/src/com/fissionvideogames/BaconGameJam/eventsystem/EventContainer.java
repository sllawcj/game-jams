package com.fissionvideogames.BaconGameJam.eventsystem;

import java.util.HashMap;
import java.util.LinkedList;

@SuppressWarnings("rawtypes")
public class EventContainer {
	private HashMap<Class, LinkedList<EventListener>> listeners = new HashMap<Class, LinkedList<EventListener>>();
	
	public void addListener(Class clss, EventListener listener) {
		if(!listeners.containsKey(clss))
			listeners.put(clss, new LinkedList<EventListener>());
		listeners.get(clss).add(listener);
	}

	public LinkedList<EventListener> getListeners(Class clss) {
		if(!listeners.containsKey(clss))
			listeners.put(clss, new LinkedList<EventListener>());
		return listeners.get(clss);
	}
}