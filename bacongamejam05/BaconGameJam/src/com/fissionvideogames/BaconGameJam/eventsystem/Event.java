package com.fissionvideogames.BaconGameJam.eventsystem;

import com.fissionvideogames.BaconGameJam.controller.GameEngine;

@SuppressWarnings("rawtypes")
public abstract class Event {

	private boolean cancelled = false;
	public GameEngine engine;
	
	public Event (GameEngine engine)
	{
		this.engine = engine;
	}
	
	public void cancel() 
	{
		cancelled = true;
	}

	@SuppressWarnings("unchecked")
	public void run() 
	{
		for(EventListener eL : engine.events.getListeners(getClass())) 
		{
			if(eL.armed) 
			{
				eL.beforeEvent(this);
			}
		}
		
		if(cancelled) 
		{
			return;
		}
		
		perform();
		for(EventListener eL : engine.events.getListeners(getClass())) 
		{
			if(eL.armed) 
			{
				eL.afterEvent(this);
			}
		}
	}

	protected abstract void perform();
}