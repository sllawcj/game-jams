package com.fissionvideogames.BaconGameJam.eventsystem;

import com.fissionvideogames.BaconGameJam.entities.Entity;

public class DamageEvent extends Event 
{

	public Entity damager, damaged;
	public int damage;
	
	public DamageEvent(Entity damager, Entity damaged, int damage) 
	{
		super(damager.engine);
		this.damager = damager;
		this.damaged = damaged;
		this.damage = damage;
		run();
	}
	
	protected void perform() {
		damager.health -= damage;
		if(damager.health <= 0) {
			damager.kill();
		}
	}
}