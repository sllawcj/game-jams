package com.fissionvideogames.BaconGameJam;

import com.badlogic.gdx.Game;
import com.fissionvideogames.BaconGameJam.controller.GameEngine;
import com.fissionvideogames.BaconGameJam.screen.GamePlay;
import com.fissionvideogames.BaconGameJam.screen.MainScreen;

public class BaseGame extends Game
{
	
	MainScreen Game;
	@Override
	public void create()
	{

		MainScreen gp= new MainScreen(this);
		//GamePlay gp = new GamePlay();
		setScreen(gp);
	}
}
