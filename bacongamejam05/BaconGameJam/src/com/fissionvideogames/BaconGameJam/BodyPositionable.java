package com.fissionvideogames.BaconGameJam;

import com.badlogic.gdx.physics.box2d.Body;

public class BodyPositionable extends Positionable
{
	public Body body;

	public BodyPositionable(Body body)
	{
		this.body = body;
	}

	public float getX()
	{
		return body.getWorldCenter().x * 32;
	}

	public float getY()
	{
		return body.getWorldCenter().y * 32;
	}

	public void setX(float x)
	{
		body.setTransform(x / 32, body.getWorldCenter().y / 32, 0);
	}

	public void setY(float y)
	{
		body.setTransform(body.getWorldCenter().x / 32, y / 32, 0);
	}
}