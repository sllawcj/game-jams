package com.fissionvideogames.BaconGameJam.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.fissionvideogames.BaconGameJam.controller.GameEngine;

public class GamePlay implements Screen, InputProcessor
{
	GameEngine gameEngine;
	
	public GamePlay ()
	{
		gameEngine = new GameEngine ();
	}
	
	@Override
	public boolean keyDown(int keycode)
	{		
		
		return false;
	}

	@Override
	public boolean keyUp(int keycode)
	{
		if(keycode == Keys.B){
			gameEngine.ToggleDebugRender();
			return true;
		}
		if(keycode == Keys.N){
			gameEngine.ToggleDrawTiles();
			return true;
		}
		if (keycode == Keys.M)
		{
			gameEngine.ToggleDrawPaths();
			return true;
		}
		if(keycode == Keys.L){
			gameEngine.ToggleAmbientLight();
			return true;
		}
		if(keycode == Keys.P){
			gameEngine.ToggleCheatPath();
			return true;
		}
		if(keycode == Keys.SPACE && (gameEngine.gameWin || gameEngine.gameLose)){
			gameEngine.running = false;
			gameEngine.reset();
			gameEngine.running = true;
		}
		
		return false;
	}

	@Override
	public boolean keyTyped(char character)
	{
		
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button)
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button)
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer)
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY)
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount)
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void render(float delta)
	{		
		
		gameEngine.update(delta);
		
		gameEngine.render(delta);
		// TODO Auto-generated method stub	
	}

	@Override
	public void resize(int width, int height)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show()
	{
		InputMultiplexer p = new InputMultiplexer();		
		p.addProcessor(0,this);
		Gdx.input.setInputProcessor(p);
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose()
	{
		// TODO Auto-generated method stub
		
	}

}
