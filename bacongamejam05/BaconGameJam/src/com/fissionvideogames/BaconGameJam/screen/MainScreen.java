package com.fissionvideogames.BaconGameJam.screen;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.fissionvideogames.BaconGameJam.BaseGame;

public class MainScreen implements Screen
{
	public static BaseGame basegame;
	SpriteBatch renderer = new SpriteBatch();
	Texture bg = new Texture(Gdx.files.internal("assets/menu/background.png"));
	Texture splash = new Texture(
			Gdx.files.internal("assets/menu/splash/Fission.png"));
	float alpha = 0f;
	boolean done;
	boolean up;
	Color color;
	Music splashSound;
	Music sound;
	Texture button = new Texture(
			Gdx.files.internal("assets/menu/playbutton.png"));
	Texture title = new Texture(Gdx.files.internal("assets/titleImg.png"));
	ArrayList<Vector2> verts1 = new ArrayList<Vector2>();
	List<Vector2> verts;

	public MainScreen(BaseGame baseGame)
	{
		this.basegame = baseGame;
		color = renderer.getColor();
		
		verts1.add((new Vector2(350, 100)));
		verts1.add(new Vector2(350 + button.getWidth(), 100));
		verts1.add(new Vector2(350 + button.getWidth(), 100 + button.getHeight()));
		verts1.add(new Vector2(350, 100 + button.getHeight()));
		verts = verts1;
		
		splashSound = Gdx.audio.newMusic(Gdx.files
				.internal("assets/music/fissionsplash.wav"));
		sound = Gdx.audio.newMusic(Gdx.files
				.internal("assets/music/fissionmenuloop.wav"));
		sound.setLooping(true);

		splashSound.play();
	}

	@Override
	public void render(float delta)
	{
		renderer.begin();

		if (!(splashSound.isPlaying()) && !sound.isPlaying())
		{
			sound.play();
			splashSound.dispose();
		}

		if (splashSound.isPlaying())
		{
			Color c = color;
			c.a = alpha;
			renderer.setColor(c);
			renderer.draw(splash, 0, 0);
			alpha += .005f;
		} else
		{
			renderer.draw(bg, 0, 0);
			renderer.draw(button, 350, 100);
			renderer.draw(title, 315, 650);
		}

		renderer.end();

		if (Gdx.input.isButtonPressed(0)
				&& Intersector.isPointInPolygon(verts,
						new Vector2(Gdx.input.getX(), Gdx.input.getY())))
		{
			sound.dispose();
			startGame();
		}
	}

	@Override
	public void resize(int width, int height)
	{
		// TODO Auto-generatwed method stub

	}

	@Override
	public void show()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void hide()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void pause()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void resume()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose()
	{
		renderer.dispose();

	}

	public void startGame()
	{
		basegame.setScreen(new GamePlay());
	}
}
