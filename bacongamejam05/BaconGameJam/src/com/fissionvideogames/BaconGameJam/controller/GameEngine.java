package com.fissionvideogames.BaconGameJam.controller;

import java.util.ArrayList;
import java.util.HashMap;

import box2dLight.RayHandler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.JointEdge;
import com.badlogic.gdx.physics.box2d.World;
import com.fissionvideogames.BaconGameJam.BaconUtil;
import com.fissionvideogames.BaconGameJam.entities.Battery;
import com.fissionvideogames.BaconGameJam.entities.Entity;
import com.fissionvideogames.BaconGameJam.entities.EntityAI;
import com.fissionvideogames.BaconGameJam.entities.Player;
import com.fissionvideogames.BaconGameJam.eventsystem.EventContainer;
import com.fissionvideogames.BaconGameJam.music.MusicGenerator;
import com.fissionvideogames.BaconGameJam.world.Map;

public class GameEngine
{
	public Map map;
	public Player player;
	public ArrayList<Entity> entities;
	public ArrayList<Entity> removeEntities;
	public RayHandler rayHandler;
	public World physics;
	public OrthographicCamera camera;
	public SpriteBatch batch;
	public EventContainer events;
	public TextureRegion playerTexture = new TextureRegion(new Texture(
			Gdx.files.internal("assets/player/playeranimationsheet.png")), 32,
			32);

	public TextureRegion batteryTexture = new TextureRegion(new Texture(
			Gdx.files.internal("assets/battery.png")), 32, 32);

	Box2DDebugRenderer debugRenderer;
	/** pixel perfect projection for font rendering */
	Matrix4 normalProjection = new Matrix4();
	private BitmapFont font;

	Music music;
	MusicGenerator musicGen;
	boolean alert; // true if enemies are alert to player

	void CreateCamera()
	{
		camera = new OrthographicCamera(48, 32);
		camera.position.set(0, 16, 0);
		camera.update();
		// camera.setToOrtho(true);
	}

	void CreateBatch()
	{
		batch = new SpriteBatch();
	}

	public static int RAYS_PER_BALL = 128;

	void CreateLighting()
	{
		RayHandler.setGammaCorrection(true);
		RayHandler.useDiffuseLight(true);
		rayHandler = new RayHandler(physics);
		rayHandler.setAmbientLight(0f, 0f, 0f, 0.1f);
		rayHandler.setCulling(true);
		// rayHandler.setBlur(false);
		rayHandler.setBlurNum(1);
		camera.update(true);
	}

	boolean ambientLightFullOn = false;

	public void ToggleAmbientLight()
	{
		if (ambientLightFullOn)
		{
			rayHandler.setAmbientLight(0f, 0f, 0f, 0.1f);
		} else
		{
			rayHandler.setAmbientLight(1f, 1f, 1f, 0.1f);
		}
		ambientLightFullOn = !ambientLightFullOn;
	}

	public void ToggleDrawPathTextures()
	{
		map.drawPathTextures = !map.drawPathTextures;
	}

	private float physicsTimeLeft;

	void CreatePhysics()
	{
		physics = new World(new Vector2(0, 0), true);
		debugRenderer = new Box2DDebugRenderer();
	}

	public GameEngine()
	{
		reset();
	}

	Vector2 lastCulledPlayerPosition;
	HashMap<Vector2, Body> shownBlocksList;

	int roomCount = 50;

	class bodyUserData
	{
		boolean markedForDelete = false;
	}

	private void removeBodySafely(Body body)
	{
		final ArrayList<JointEdge> list = body.getJointList();
		while (list.size() > 0)
		{
			physics.destroyJoint(list.get(0).joint);
		}
		// actual remove
		physics.destroyBody(body);
	}

	private final static int MAX_FPS = 30;
	private final static int MIN_FPS = 15;
	public final static float TIME_STEP = 1f / MAX_FPS;
	private final static float MAX_STEPS = 1f + MAX_FPS / MIN_FPS;
	private final static float MAX_TIME_PER_FRAME = TIME_STEP * MAX_STEPS;
	private final static int VELOCITY_ITERS = 6;
	private final static int POSITION_ITERS = 2;

	void PhysicsStep(float dt)
	{
		physicsTimeLeft += dt;
		if (physicsTimeLeft > MAX_TIME_PER_FRAME)
			physicsTimeLeft = MAX_TIME_PER_FRAME;

		boolean stepped = false;
		while (physicsTimeLeft >= TIME_STEP)
		{
			physics.step(TIME_STEP, VELOCITY_ITERS, POSITION_ITERS);
			physicsTimeLeft -= TIME_STEP;
			stepped = true;
		}

		if (stepped)
		{

			rayHandler.update();
			camera.position.set(new Vector3(player.body.getPosition().x,
					player.body.getPosition().y, 0));
		}

	}

	public void update(float dt)
	{
		// Gdx.app.log("Player xy",
		// player.body.getPosition().x+","+player.body.getPosition().y);
		for (int i = entities.size() - 1; i >= 0; --i)
		{
			entities.get(i).update(dt);
		}

		PhysicsStep(dt);

		// if (Gdx.input.isKeyPressed(Keys.P))
		// {
		// alert = true;
		// } else if (Gdx.input.isKeyPressed(Keys.L))
		// {
		// alert = false;
		// }

		// System.out.println(alert);
		musicUpdate();

		updateLightPower(dt);

		// System.out.println(player.playerLightPower);

		entities.removeAll(removeEntities);
		removeEntities.clear();

	}

	private void updateLightPower(float delta)
	{
		if (player.lightOn)
		{
			if (player.playerLightPower > 0)
			{
				player.playerLightPower -= .05;
			}

			player.setLightLength();
		}
	}

	public void reset()
	{
		events = new EventContainer();
		entities = new ArrayList<Entity>();
		removeEntities = new ArrayList<Entity>();
		CreateCamera();
		CreatePhysics();
		CreateLighting();
		CreateBatch();
		map = new Map(this);
		font = new BitmapFont();
		int[] startend = map.generate();
		Vector2 properStart = new Vector2(2, 2);// new Vector2(startend[0] * 2,
												// startend[1] * 2);
		player = new Player(properStart, this);

		FixtureDef fixtureDef = new FixtureDef();
		CircleShape shape = new CircleShape();

		lastCulledPlayerPosition = new Vector2(player.body.getPosition().x,
				player.body.getPosition().y);
		shownBlocksList = new HashMap<Vector2, Body>();
		Gdx.gl.glClearColor(0, 0, 0, 1);// black

		//EntityAI ai = new EntityAI(new Vector2(6, 6), this);
		// BaconUtil.createStaticRectangle(-100, -64, 64, 64, this);
		// DoCulling();

		musicGen = new MusicGenerator(Gdx.files.internal("assets/music"));

		normalProjection.setToOrtho2D(0, 0, Gdx.graphics.getWidth(),
				Gdx.graphics.getHeight());
		// musicGen = new MusicGenerator(new FileHandle("assets/music"));
		timespent = 0f;
		gameWin = false;
		gameLose = false;
	}

	private void musicUpdate()
	{
		if (music != null)
		{
			if (music.isPlaying())
			{
				return;
			} else
			{
				if (alert)
				{
					music = musicGen.getNextEpic();
					music.play();
				} else
				{
					music = musicGen.getNextAmbient();
					music.play();
				}
			}
		} else
		{
			music = musicGen.getNextAmbient();
		}
	}

	float timespent = 0f;
	public boolean gameWin = false;
	public boolean gameLose = false;
	public boolean running = true;

	public void render(float dt)
	{
		if (!running)
			return;
		camera.update();
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		batch.setProjectionMatrix(camera.combined);
		batch.disableBlending();
		batch.begin();
		batch.enableBlending();

		// final Vector2 position = testCircleBody.getPosition();
		// final float angle = MathUtils.radiansToDegrees *
		// testCircleBody.getAngle();
		// batch.draw(Art.phtextureblu, position.x - radius, position.y -
		// radius, radius, radius, radius * 2, radius * 2, 1, 1, angle);
		map.Draw();
		batch.draw(player.getFrame(), player.body.getPosition().x - 1,
				player.body.getPosition().y - 1, 1, 1, 2, 2, 1, 1,
				Math.abs(player.getDirection().x - player.getDirection().y));

		/*
		 * for(Entity e : entities) { if(e instanceof EntityAI) { try {
		 * for(GridLocation l : ((EntityAI) e).path.getList()) {
		 * batch.draw(blockTexture, l.getX() - 1, l.getY() - 1, l.getX(),
		 * l.getY(), 2, 2, 1, 1, 0); //System.out.println(l.getX() + ", " +
		 * l.getY()); } } catch(Exception exc) { //exc.printStackTrace(); } } }
		 */

		for (Entity e : entities)
		{
			if (e instanceof Battery)
			{
				batch.draw(batteryTexture, e.body.getPosition().x - 1,
						e.body.getPosition().y - 1, e.body.getPosition().x,
						e.body.getPosition().y, 2, 2, 1, 1, 0);
			}
		}

		batch.end();

		/** BOX2D LIGHT STUFF BEGIN */

		rayHandler.setCombinedMatrix(camera.combined, camera.position.x,
				camera.position.y, camera.viewportWidth * camera.zoom,
				camera.viewportHeight * camera.zoom);

		// rayHandler.setCombinedMatrix(camera.combined);

		rayHandler.render();
		if (useDebugRender)
			debugRenderer.render(physics, camera.combined);

		if (drawPaths)
		{
			for (Entity e : entities)
			{
				if (e instanceof EntityAI)
				{
					// ((EntityAI)e).drawPath();
				}
			}
		}

		// map.throughPathGen.throughPath[ map.throughPathGen.endy][
		// map.throughPathGen.endx]
		double distx = ((map.throughPathGen.endx * 2) - player.body
				.getPosition().x);
		double disty = ((map.throughPathGen.endy * 2) - player.body
				.getPosition().y);
		double distxy = Math.abs(Math.sqrt(distx * distx + disty * disty));
		if (distxy < 7 && !gameWin)
		{
			gameWin = true;
		}
		if (player.playerLightPower <= 0 && !gameWin)
			gameLose = true;
		if (!gameWin && !gameLose)
			timespent += dt;

		/** FONT */
		batch.setProjectionMatrix(normalProjection);
		batch.begin();
		String distToEnd = " toEnd:" + distxy;
		String resultString = "Time spent in maze:" + timespent + ", "
				+ distToEnd;
		if (gameWin)
			resultString = "You've won in " + timespent
					+ ", press space to try again";
		if (gameLose)
			resultString = "You've lost in " + timespent
					+ ", press space to try again";

		font.draw(batch, resultString, 1, camera.viewportHeight - 10);

		batch.end();
	}

	boolean useDebugRender = false;
	boolean drawTiles = true;
	boolean drawPaths = false;
	boolean fullAmbientLights = false;

	public void ToggleDebugRender()
	{
		// useDebugRender = !useDebugRender;
	}

	public void ToggleDrawTiles()
	{
		// drawTiles = !drawTiles;
	}

	public void ToggleDrawPaths()
	{
		// drawPaths = !drawPaths;
	}

	public void ToggleFullAmbientLights()
	{
		// fullAmbientLights =! fullAmbientLights;
		// if(fullAmbientLights)
		// rayHandler.setAmbientLight(1, 1, 1, 0.1f);
		// else
		// rayHandler.setAmbientLight(0f, 0f, 0f, 0.1f);
	}

	public void ToggleCheatPath()
	{
		// map.drawPathTextures = !map.drawPathTextures;
	}

}
