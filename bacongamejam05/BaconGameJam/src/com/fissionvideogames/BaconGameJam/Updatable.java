package com.fissionvideogames.BaconGameJam;

public interface Updatable {
	public abstract void update(float delta);
}