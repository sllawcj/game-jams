package com.fissionvideogames.BaconGameJam;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.fissionvideogames.BaconGameJam.controller.GameEngine;

public class BaconUtil 
{
	public static Body createCircleBody(float x, float y, float radius, GameEngine engine) 
	{
		CircleShape ballShape = new CircleShape();
        ballShape.setRadius(radius / 32);
        
		FixtureDef def = new FixtureDef();
        def.restitution = 0.9f;
        def.friction = 0.01f;
        def.shape = ballShape;
        def.density = 1f;
        BodyDef boxBodyDef = new BodyDef();
        boxBodyDef.type = BodyType.DynamicBody;
        
        boxBodyDef.position.x = x;
        boxBodyDef.position.y = y;
        Body body = engine.physics.createBody(boxBodyDef);
        body.createFixture(def);
        
        return body;
	}
	
	public static Body createStaticRectangle(float x, float y, float width, float height, GameEngine engine) 
	{
		PolygonShape ps = new PolygonShape();
	    ps.setAsBox(width / 2, height / 2);
		FixtureDef def = new FixtureDef();
        def.restitution = 0.9f;
        def.friction = 0.01f;
        def.shape = ps;
        def.density = 1f;
        BodyDef boxBodyDef = new BodyDef();
        boxBodyDef.type = BodyType.StaticBody;
        boxBodyDef.position.x = x + width/2;
        boxBodyDef.position.y = y + height/2;
        Body body = engine.physics.createBody(boxBodyDef);
        body.createFixture(def);
        
        return body;
	}
	
	private static Vector2 velchange = new Vector2();
	private static Vector2 impulse = new Vector2();
	public static void applyWalkingImpulse(Body body, byte walkingDirection, float speed) {
		Vector2 vel = body.getLinearVelocity();
	    float desiredX = 0, desiredY = 0;
	    switch (walkingDirection)
	    {
	      case 0: desiredY = speed; break;
	      case 1: desiredX = speed; break;
	      case 2: desiredY = -speed; break;
	      case 3: desiredX = -speed;
	    }
	    velchange.set(desiredX - vel.x, desiredY - vel.y);
	    velchange.mul(body.getMass());
	    body.applyLinearImpulse(velchange, body.getWorldCenter(), true);
	}
	
	public static float clamp (float val, float min, float max)
	{
		if (val < min)
			val = min;
		if (val > max)
			val = max;
		return val;
	}
	
	public static float scale (float val, float size)
	{
		return val * size / Math.abs(val);
	}
	
	public static float scaleback (float val, float size)
	{
		if (Math.abs(val) > size)
			val = scale(val,size);
		return val;
	}
	
	public static float signedAngle(Vector2 v1, Vector2 v2)
	{
	      float perpDot = v1.x * v2.y - v1.y * v2.x;
	 
	      return (float)Math.atan2(perpDot, v1.dot(v2));
	}
	
	public static Vector2 project (Vector2 vec, Vector2 onto)
	{
		Vector2 result = new Vector2();
		result.set(onto);
		result.nor();
		result.scl(vec.dot(onto)/vec.len()/onto.len());
		return result;
	}
	
	
}