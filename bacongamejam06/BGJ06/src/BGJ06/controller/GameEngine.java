package BGJ06.controller;

import java.util.ArrayList;

import BGJ06.entities.Entity;
import BGJ06.entities.Player;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;

public class GameEngine
{
	public OrthographicCamera camera;
	public SpriteBatch batch;
	public World physics;
	Box2DDebugRenderer debugRenderer;
	public Player player;
	public ArrayList<Entity> entities = new ArrayList<Entity>();

	private float physicsTimeLeft;
	private final static int MAX_FPS = 30;
	private final static int MIN_FPS = 15;
	public final static float TIME_STEP = 1f / MAX_FPS;
	private final static float MAX_STEPS = 1f + MAX_FPS / MIN_FPS;
	private final static float MAX_TIME_PER_FRAME = TIME_STEP * MAX_STEPS;
	private final static int VELOCITY_ITERS = 6;
	private final static int POSITION_ITERS = 2;
	Matrix4 normalProjection = new Matrix4();

	void CreateCamera()
	{
		camera = new OrthographicCamera(800, 600);
		camera.position.set(0, 16, 0);
		camera.update();
	}

	void CreateBatch()
	{
		batch = new SpriteBatch();
	}

	void CreatePhysics()
	{
		physics = new World(new Vector2(0, 0), true);
		debugRenderer = new Box2DDebugRenderer();
	}

	void PhysicsStep(float dt)
	{
		physicsTimeLeft += dt;
		if (physicsTimeLeft > MAX_TIME_PER_FRAME)
			physicsTimeLeft = MAX_TIME_PER_FRAME;

		boolean stepped = false;
		while (physicsTimeLeft >= TIME_STEP)
		{
			physics.step(TIME_STEP, VELOCITY_ITERS, POSITION_ITERS);
			physicsTimeLeft -= TIME_STEP;
			stepped = true;
		}

		if (stepped)
		{

			camera.position.set(new Vector3(player.body.getPosition().x,
					player.body.getPosition().y, 0));
		}
	}

	public GameEngine()
	{
		CreateCamera();
		CreateBatch();
		CreatePhysics();
		
		player = new Player(new Vector2(0,0), this);
		
        normalProjection.setToOrtho2D(0, 0, Gdx.graphics.getWidth(),
                Gdx.graphics.getHeight());
	}

	public void update(float dt)
	{
		PhysicsStep(dt);
	}

	public void render(float delta)
	{
		camera.update();
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		batch.setProjectionMatrix(camera.combined);
		batch.disableBlending();
		batch.begin();
		batch.enableBlending();
		
		batch.draw(new Texture("resources/images/Untitled.png"), 0, 0);
		
		batch.draw(player.getFrame(), player.body.getPosition().x - 1,
				player.body.getPosition().y - 1, 1, 1, 64, 64, 1, 1,
				Math.abs(player.getDirection().x - player.getDirection().y));
		batch.end();
	}

	public void dispose()
	{
		batch.dispose();
	}
}
