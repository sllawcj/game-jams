package BGJ06;

import BGJ06.screens.GamePlay;
import BGJ06.screens.Mainscreen;

import com.badlogic.gdx.Game;

public class BaseGame extends Game
{
	Mainscreen Game;
	
	@Override
	public void create()
	{
		//Mainscreen gp = new Mainscreen(this); //TODO create the main screen
		GamePlay gp = new GamePlay();
		
		setScreen(gp);
	}
}

