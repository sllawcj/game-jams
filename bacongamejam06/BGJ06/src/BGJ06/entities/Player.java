package BGJ06.entities;

import BGJ06.controller.GameEngine;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class Player extends Entity
{

	private static final int FRAME_COLS = 1;
	private static final int FRAME_ROWS = 1;
	public Texture walkSheet;
	TextureRegion[] walkFrames;
	Animation walkAnimation;
	float stateTime;
	TextureRegion currentFrame;

	public Player(Vector2 pos, GameEngine engine)
	{
		super(100, 33, 33, pos, engine);
		loadAnimations();
	}

	private void loadAnimations()
	{
		walkSheet = new Texture(
				Gdx.files.internal("resources/images/PC.png"));
		TextureRegion[][] tmp = TextureRegion.split(walkSheet,
				walkSheet.getWidth() / FRAME_COLS, walkSheet.getHeight()
						/ FRAME_ROWS); // #10
		walkFrames = new TextureRegion[FRAME_COLS * FRAME_ROWS];
		int index = 0;
		for (int i = 0; i < FRAME_ROWS; i++)
		{
			for (int j = 0; j < FRAME_COLS; j++)
			{
				walkFrames[index++] = tmp[i][j];
			}
		}
		walkAnimation = new Animation(.375f, walkFrames); // #11
		stateTime = 0f;
	}

	public boolean stopPlayerUpdatingCharacter = false;

	public void update(float delta)
	{
		body.setTransform(
				body.getTransform().getPosition(),
				(float) (Math.atan2(Gdx.input.getX() - Gdx.graphics.getWidth()
						/ 2, Gdx.input.getY() - Gdx.graphics.getHeight() / 2) - Math.PI / 2));

		Vector2 dir = new Vector2(0, 0);
		if (!stopPlayerUpdatingCharacter)
		{
			if (Gdx.input.isKeyPressed(Input.Keys.W))
			{
				dir.y += 1;
			}
			if (Gdx.input.isKeyPressed(Input.Keys.A))
			{
				dir.x += -1;
			}
			if (Gdx.input.isKeyPressed(Input.Keys.S))
			{
				dir.y += -1;
			}
			if (Gdx.input.isKeyPressed(Input.Keys.D))
			{
				dir.x += 1;
			}
		}

		if (dir.len() != 0)
			dir.mul(1.0f / dir.len());

		walk(dir);
	}

	protected float getCollisionRadius()
	{
		return 26;
	}

	public void onDeath()
	{

	}

	public TextureRegion getFrame()
	{
		stateTime += Gdx.graphics.getDeltaTime();
		currentFrame = walkAnimation.getKeyFrame(stateTime, true);
		return currentFrame;
	}

}