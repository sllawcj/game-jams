package BGJ06.entities.utils;

public interface Updatable {
        public abstract void update(float delta);
}