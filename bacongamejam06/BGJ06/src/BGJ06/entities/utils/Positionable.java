package BGJ06.entities.utils;

public abstract class Positionable {
    public void setLocation(float x, float y) {
            setX(x);
            setY(y);
    }
    
    public abstract float getX();
    public abstract float getY();
    public abstract void setX(float x);
    public abstract void setY(float y);
}